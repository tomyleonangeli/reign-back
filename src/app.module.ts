/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArticleModule } from './article/article.module';


@Module({
  imports: [ArticleModule,MongooseModule.forRoot('mongodb://localhost/articles')],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
