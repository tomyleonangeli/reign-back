/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Res } from '@nestjs/common';
import { ArticleService } from './article.service';
import { articleDTO } from './dto/aticle.dto';


@Controller('/article')
export class ArticleController {

    constructor(private article: ArticleService) {

    }
    @Post()
    async create(@Body() articles, @Res() res) {
        try {
            for (let i = 0; articles.length > i; i++) {
                await this.article.saveArticle(articles[i]);
            }

            return res.status(HttpStatus.OK).json({ message: 'done' });
        } catch (error) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: error });
        }
    }


    @Get()
    async findAll(@Res() res) {
        const articles = await this.article.getArticles();
        return res.status(HttpStatus.OK).json(articles)
    }

    @Delete('/:id')
    async deleteAll(@Param() article, @Res() res) {

        await this.article.deleteArticle(article.id);

        return res.status(HttpStatus.OK).json({ message: 'done' })
    }
}
