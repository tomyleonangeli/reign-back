/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Article } from './interfaces/article.interface';
@Injectable()
export class ArticleService {

    constructor(@InjectModel('Article') private readonly articleModel: Model<Article>) { }

    async getArticles(): Promise<Article[]> {
        return await this.articleModel.find().sort({created_at: -1});
    }

    async saveArticle(article): Promise<string> {
        await this.articleModel.create(article);
        return 'OK';
    }

    async deleteArticle(id): Promise<string> {
        await this.articleModel.findByIdAndDelete(id);
        return 'Done';
    }

}
