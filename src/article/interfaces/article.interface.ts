/* eslint-disable prettier/prettier */
import { Document } from 'mongoose';
export interface Article extends Document {
    readonly author: string;
    readonly title: string;
    readonly created_at: Date;
    readonly url: string,
    readonly story_title: string,
    readonly story_url: string
}


