/* eslint-disable prettier/prettier */
import { Schema } from 'mongoose';

export const ArticleSchema = new Schema({
    author: String,
    title: String,
    created_at: Date,
    url: String,
    story_title: String,
    story_url: String
});

